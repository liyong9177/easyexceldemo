package com.mi.easyexceldemo.vo;


import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mi.easyexceldemo.annotation.CityValid;
import com.mi.easyexceldemo.annotation.Sensitive;
import com.mi.easyexceldemo.enums.SensitiveType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@Data
@ApiModel(value = "个人信息")
public class People implements Serializable {
    @ApiModelProperty(value = "id")
    protected int id;

    @Sensitive(SensitiveType.NAME)
    @ExcelProperty(value = "姓名", index = 0)
    @ApiModelProperty(value = "姓名")
    protected String name;

    @Sensitive(SensitiveType.SEX)
    @ExcelProperty(value = "性别", index = 1)
    @ApiModelProperty(value = "性别")
    protected String sex;

    @Sensitive(SensitiveType.MOBILE)
    @Size(min = 11, max = 11, message = "请输入正确的手机号")
    @ExcelProperty(value = "联系人手机", index = 2)
    @ApiModelProperty(value = "联系人手机")
    protected String Phone;

    @Sensitive(SensitiveType.IDCARD)
    @ExcelProperty(value = "证件号码", index = 3)
    @ApiModelProperty(value = "证件号码")
    protected Long idCard;

    @ExcelProperty(value = "地址", index = 4)
    @ApiModelProperty(value = "地址")
    protected String address;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    protected Date createDate;

    @ApiModelProperty(value = "联系人所在省id")
    private Long provinceId;

    @CityValid(fieldName = "cityName", city = false, message = "联系人所在市不在该省内")
    @ExcelProperty(value = "联系人所在省名称", index = 5)
    @ApiModelProperty(value = "联系人所在省名称")
    private String provinceName;

    @ApiModelProperty(value = "联系人所在市id")
    private Long cityId;

    @CityValid(fieldName = "provinceName", city = true, message = "该城市不属于联系人所在省")
    @ExcelProperty(value = "联系人所在市名称", index = 6)
    @ApiModelProperty(value = "联系人所在市名称")
    private String cityName;
}
