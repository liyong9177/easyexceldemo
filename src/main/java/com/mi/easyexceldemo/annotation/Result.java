package com.mi.easyexceldemo.annotation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.apache.http.HttpStatus;

import java.io.Serializable;

@Data
@ToString
@ApiModel(value = "返回类")
public class Result<T> implements Serializable {

    @JsonIgnore
    private final String success = "success";

    @ApiModelProperty(value = "状态码")
    private Integer status;

    @ApiModelProperty(value = "描述")
    private String message;

    @ApiModelProperty(value = "对象")
    private T data;


    public Result() {
        super();
    }

    public Result(T data, String message, Integer status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public Result(T data) {
        this.data = data;
        this.message = success;
        this.status = HttpStatus.SC_OK;
    }

}