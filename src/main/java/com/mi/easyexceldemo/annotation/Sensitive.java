package com.mi.easyexceldemo.annotation;

import com.mi.easyexceldemo.enums.SensitiveType;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target({ElementType.FIELD, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
public @interface Sensitive {
    SensitiveType value();
}
