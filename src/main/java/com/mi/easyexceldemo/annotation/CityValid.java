package com.mi.easyexceldemo.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target({ElementType.FIELD, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
public @interface CityValid {

    /**
     * 错误提示信息
     */
    String message() default "";

    /**
     * 关联的属性名称
     */
    String fieldName();

    /**
     * 市：true,省：false
     */
    boolean city();

}
