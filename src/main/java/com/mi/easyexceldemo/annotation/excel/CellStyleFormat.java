package com.mi.easyexceldemo.annotation.excel;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import java.lang.annotation.*;


/**
 * 可以标注在属性上和类上，标注在类上表示该类内所有属性都这个样式，标注在属性上，表示该属性是这个样式 如果同时标注，则属性的样式覆盖类的样式
 *
 * @author sunshuai
 * @date 2019-07-25 16:18
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface CellStyleFormat {

  /**
   * 水平居中方式 默认居中
   *
   * @see HorizontalAlignment
   */
  HorizontalAlignment horizontalAlignment() default HorizontalAlignment.CENTER;

  /**
   * 垂直对齐方式，默认居中
   *
   * @see VerticalAlignment
   */
  VerticalAlignment verticalAlignment() default VerticalAlignment.CENTER;

  /**
   * 字体设置
   *
   * @see org.apache.poi.xssf.usermodel.XSSFFont
   * @see org.apache.poi.hssf.usermodel.HSSFFont
   */
  CellFontFormat cellFont() default @CellFontFormat();


  /**
   * 背景颜色
   *
   * @see IndexedColors
   */
  IndexedColors fillBackgroundColor() default IndexedColors.WHITE;

  /**
   * 底部边框，默认没有
   */
  BorderStyle borderBottom() default BorderStyle.THIN;

  /**
   * 左侧边框，默认没有
   */
  BorderStyle borderLeft() default BorderStyle.THIN;

  /**
   * 右侧边框，默认没有
   */
  BorderStyle borderRight() default BorderStyle.THIN;

  /**
   * 顶部边框，默认没有
   */
  BorderStyle borderTop() default BorderStyle.THIN;

  boolean wrapText() default true;

}
