package com.mi.easyexceldemo.annotation.excel;

import java.lang.annotation.*;

/**
 * 行样式设定，目前只支持行高，放到类的上面
 *
 * @author sunshuai
 * @date 2019-07-24 17:17
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RowStyleFormat {

  /**
   * 行高，单位磅，默认20磅
   */
  float heightInPoints() default 20f;

  /**
   * 第几行应用样式，默认0
   */
  int rowNum() default 0;

}
