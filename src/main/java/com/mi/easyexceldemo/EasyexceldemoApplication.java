package com.mi.easyexceldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyexceldemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyexceldemoApplication.class, args);
    }

}
