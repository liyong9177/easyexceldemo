package com.mi.easyexceldemo.service;

import com.mi.easyexceldemo.constants.ComConstants;
import com.mi.easyexceldemo.utils.CommonUtil;
import com.mi.easyexceldemo.vo.People;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class FilerMeaasgeService {
    public String importExcelData(String filePath) throws Exception {
        //定义需要校验的字段
        Map<String, String[]> checkRules = ComConstants.USER_CHECK_RULES;
        //header
        String[] fieldNames = ComConstants.USER_COMPLAINT;
        Class<People> clazz = People.class;
        Map<String, Object> checkResult = CommonUtil
                .convertImportData(filePath, checkRules, fieldNames, clazz);
        String msg = (String) checkResult.get("msg");
        boolean chkflg = (boolean) checkResult.get("chkflg");
        List<People> entityList = (List<People>) checkResult
                .get("entityList");
        if (chkflg && entityList.size() > 0) {
            System.out.println(entityList.toString());
        }
        return msg;
    }
}
