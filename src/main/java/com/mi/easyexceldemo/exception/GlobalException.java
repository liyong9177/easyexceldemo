package com.mi.easyexceldemo.exception;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GlobalException extends RuntimeException {

    private Integer status;

    public GlobalException(int status, String msg) {
        super(msg);
        this.status = status;
    }

    public GlobalException(String msg) {
        super(msg);
        this.status = 500;
    }

}