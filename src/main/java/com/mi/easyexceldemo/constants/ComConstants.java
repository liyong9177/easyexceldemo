package com.mi.easyexceldemo.constants;

import java.util.HashMap;
import java.util.Map;

public class ComConstants {
    public static final String DATE_FORMAT_STANDARD = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_SHORT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_YMDHMS = "yyyyMMddHHmmss";

    public static final String NUMBER_FORMAT_THREE = "##.###%";
    public static final String NUMBER_FORMAT_TWO = "##.##%";

    public static final String BROWSER_TYPE_FIREFOX = "firefox";
    public static final Map<String, String[]> USER_CHECK_RULES = new HashMap<String, String[]>(){
        private static final long serialVersionUID = 1L;
        {
            put("requireField", new String[]{"name","sex","Phone","provinceName", "cityName"});
            put("dateField", new String[]{"createDate"});
            put("dictField", new String[]{"address"});
            put("nonRepeat", new String[]{"idCard"});
        }
    };
    public static final String[] USER_COMPLAINT = new String[]{
            "name","sex","Phone","provinceName", "cityName","createDate","idCard","address"
    };
}
