package com.mi.easyexceldemo.dao;

import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

import java.util.Map;

@Mapper
public interface ProvinceCityDao {
    Map<String, Object> getByProvinceAndCity(@Param("provinceName") String provinceName,
                                             @Param("cityName") String cityName);
}
