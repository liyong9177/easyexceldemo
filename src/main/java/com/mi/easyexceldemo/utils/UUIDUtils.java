package com.mi.easyexceldemo.utils;

import java.util.UUID;

public class UUIDUtils {


    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 最低有效 64 位
     */
    public static long getLeastLong(){
        return UUID.randomUUID().getLeastSignificantBits();
    }

    /**
     * 最高有效 64 位
     */
    public static long getMostLong(){
        return UUID.randomUUID().getMostSignificantBits();
    }

}