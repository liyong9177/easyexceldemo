package com.mi.easyexceldemo.utils;

import com.mi.easyexceldemo.constants.ComConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateStringUtil {

    public static String DATE_FORMAT_STANDARD = "yyyy-MM-dd HH:mm:ss";
    public static String DATE_FORMAT_SHORT_SHORT = "yyyy-MM-dd";

    public static String getCurrentDateString(String format) {
        if (StringUtils.isBlank(format)) {
            format = ComConstants.DATE_FORMAT_STANDARD;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }

    public static String getFormatDateString(String dateStr, String format) {
        if (StringUtils.isBlank(format)) {
            format = ComConstants.DATE_FORMAT_STANDARD;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.format(DateUtils.parseDate(dateStr,
                    Locale.CHINA, "yyyy-MM-dd HH:mm:ss", "yyyy/MM/dd HH:mm:ss",
                    "yyyyMMdd HH:mm:ss"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date getFormatDate(String dateStr, String format) {
        if (StringUtils.isBlank(format)) {
            format = ComConstants.DATE_FORMAT_STANDARD;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isNotDate(String str) {
        try {
            DateUtils.parseDate(str, Locale.CHINA, "yyyy-MM-dd HH:mm:ss", "yyyy/MM/dd HH:mm:ss",
                    "yyyyMMdd HH:mm:ss");
            return false;
        } catch (ParseException e) {
            return true;
        }
    }

    public static String complementTime(String dateStr) {
        StringBuilder builder = new StringBuilder(dateStr);
        int length = dateStr.split(" ").length;
        int length1 = dateStr.split(":").length;
        if (length == 1) {
            builder.append(" 00:00:00");
        } else {
            if (length1 == 1) {
                builder.append(":00:00");
            } else if (length1 == 2) {
                builder.append(":00");
            }
        }
        return builder.toString();
    }
}
