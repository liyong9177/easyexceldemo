package com.mi.easyexceldemo.enums;

public enum SensitiveType {
    /** 手机号 */
    MOBILE,

    /** 身份证号 */
    IDCARD,

    /** 姓名 */
    NAME,

    /** 住址 */
    ADDRESS,
    /** 性别 **/
    SEX
}
