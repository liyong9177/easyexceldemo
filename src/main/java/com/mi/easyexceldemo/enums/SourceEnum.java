package com.mi.easyexceldemo.enums;

public enum SourceEnum {
    SOURCE_AMERICA("America", "美国"),
    SOURCE_CHINA("China", "中国"),
    SOURCE_JAPAN("Japan", "日本");


    private final String code;
    private final String name;

    SourceEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static SourceEnum select(String name) {
        for (SourceEnum recallSourceEnum : SourceEnum.values()) {
            if (recallSourceEnum.getName().equals(name)) {
                return recallSourceEnum;
            }
        }
        return null;
    }

    public static String[] getNameArray() {
        SourceEnum[] enumArr = SourceEnum.values();
        String[] names = new String[enumArr.length];
        for (int i = 0; i < enumArr.length; i++) {
            names[i] = enumArr[i].getName();
        }
        return names;
    }
}
