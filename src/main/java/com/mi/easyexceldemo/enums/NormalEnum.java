package com.mi.easyexceldemo.enums;

import org.apache.commons.lang3.StringUtils;

public enum NormalEnum {

    /**
     *
     */
    YES("1", "是"),
    NO("0", "否");

    private final String code;
    private final String name;

    NormalEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    //通过key 来找到相应的枚举类
    public static NormalEnum select(String code) {
        for (NormalEnum normalEnum : NormalEnum.values()) {
            if (normalEnum.getCode().equals(code)) {
                return normalEnum;
            }
        }
        return null;
    }

    /**
     * 通过名称找到枚举类
     * @author sunshuai
     * @date 2019-04-23 17:11
     */
    public static NormalEnum getByName(String name) {
        for (NormalEnum normalEnum : NormalEnum.values()) {
            if (StringUtils.equals(normalEnum.getName(), name)) {
                return normalEnum;
            }
        }
        return null;
    }

    public static String[] getNameArray() {
        NormalEnum[] enumArr = NormalEnum.values();
        String[] names = new String[enumArr.length];
        for (int i = 0; i < enumArr.length; i++) {
            names[i] = enumArr[i].getName();
        }
        return names;
    }
}
